#pragma once
#include <stdio.h>
#include <Arduino.h>
#include <rhio-pinmap.h>

void unittest_uart_begin() { Serial.begin(9600); }

void unittest_uart_putchar(char c) { Serial.write(c); }

void unittest_uart_flush() { Serial.flush(); }

void unittest_uart_end() {}
